<?php

// You can get these details from here: https://apps.twitter.com/app/new
define('TWITTER_API_KEY', 'PUT YOUR KEY HERE');
define('TWITTER_API_SECRET', 'PUT YOUR KEY HERE');
define('TWITTER_ACCESS_TOKEN', 'PUT YOUR KEY HERE');
define('TWITTER_ACCESS_SECRET', 'PUT YOUR KEY HERE');

// A query to search Twitter with. This is basically their advanced search feature
// which you can find here: https://twitter.com/search-advanced
define('SEARCH_Q', 'win "RT to win" OR "Retweet to win" -congratulations');

// Number of requests to make per execution, you can make up to 450 per 15 minutes, but try to keep this lower,
// around 5-10 to be safe as you need to consider rate limits for retweets/favourites etc.
// More information here: https://dev.twitter.com/rest/public/rate-limits
define('MAX_REQUESTS', 5);

// ------------------------------------------------------











// ------------------------------------------------------

define('BASE_PATH', __DIR__);

// Composer
require_once(__DIR__ . '/vendor/autoload.php');

// New scraper
$scrape = new Malcolm\Scrape;

// Grab some tweets
$tweets = $scrape->get();

// New tweeter
$tweeter = new Malcolm\Tweeter;

$tweeter->process($tweets);