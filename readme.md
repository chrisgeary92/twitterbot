# Automatic Twitter Bot

After checking out [this post](http://www.unilad.co.uk/articles/guy-wins-sht-load-of-twitter-competitions-thanks-to-this-creation/) on UNILAD on a Friday afternoon, I decided to have a crack at my own script.

## Disclaimer

This was an educational project and the code is published purely for education purposes. At no point should you attempt to use this script to automate your own Twitter accounts, doing so will break Twitter terms of use. The Twitter API [automation rules](https://support.twitter.com/articles/76915) prohibits automation of retweets, favourites follows/unfollows.

## Requirements

Your environment must be running [PHP](http://php.net/) 5.4 or greater, and support CURL.

## Setup

### API Keys

Setup is pretty simple. Open cron.php, and include your own Twitter API details on lines 4-8. These can be [created here](https://apps.twitter.com/app/new). You will need both application, and user access keys.

### Cron Task

In order to run this script automatically, you'll need to setup an automated task via Cron. You can also use [Easy Cron](https://www.easycron.com/) if you're looking for a simpler solution.

    */15 * * * * php /path/to/directory/cron.php 1>> /dev/null 2>&1

## License

The MIT License (MIT)

Copyright (c) 2015 Christopher Geary

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.