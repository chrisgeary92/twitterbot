<?php

namespace Malcolm;

use Abraham\TwitterOAuth\TwitterOAuth;

class Scrape
{
    /**
     * @var TwitterOAuth
     */
    protected $oauth;

    /**
     * New scraper.
     *
     */
    public function __construct()
    {
        $this->oauth = new TwitterOAuth(TWITTER_API_KEY, TWITTER_API_SECRET);
    }

    /**
     * Get some tweets.
     *
     * @return array
     */
    public function get()
    {
        $data = $this->request();

        $collection = [];

        foreach ($data as $tweets) {
            foreach ($tweets as $tweet) {
                if ($this->shouldSkipTweet($tweet)) {
                    continue;
                }

                $collection[$tweet->id] = array(
                    'user' => $tweet->user->screen_name,
                    'status' => $tweet->text
                );
            }
        }

        return $collection;
    }

    /**
     * Make the request(s) to Twitter.
     *
     * @return array
     */
    protected function request()
    {
        $since_id = file_get_contents(BASE_PATH . '/cache.txt') ?: 0;

        $tweets = $this->oauth->get('search/tweets', [
            'q' => urlencode(SEARCH_Q),
            'count' => 100,
            'since_id' => $since_id
        ]);

        @file_put_contents(BASE_PATH . '/cache.txt', $tweets->search_metadata->max_id);

        $data = [];
        $data[] = $tweets->statuses;

        for ($i = 0; $i < (MAX_REQUESTS - 1); $i++) {

            $max_id = end($tweets->statuses)->id;
            $tweets = $this->oauth->get('search/tweets', [
                'q' => urlencode(SEARCH_Q),
                'since_id' => $since_id,
                'max_id' => ($max_id - 1),
                'count' => 100
            ]);

            if (0 === count($tweets->statuses)) {
                break;
            }

            $data[] = $tweets->statuses;
        }

        return $data;
    }

    /**
     * Forget the tweet if it does not meet basic criteria.
     *
     * @param $tweet
     * @return bool
     */
    public function shouldSkipTweet($tweet)
    {
        $text = trim(mb_strtolower($tweet->text));

        if (0 === strlen($text) || 0 === strpos($text, 'rt @')) {
            return true;
        }

        return false;
    }
}