<?php

namespace Malcolm;

use Abraham\TwitterOAuth\TwitterOAuth;

class Tweeter
{
    /**
     * @var TwitterOAuth
     */
    protected $oauth;

    /**
     * Number of requests as a user.
     *
     * @var int
     */
    protected $requests = 0;

    /**
     * New tweeter.
     *
     */
    public function __construct()
    {
        $this->oauth = new TwitterOAuth(
            TWITTER_API_KEY,
            TWITTER_API_SECRET,
            TWITTER_ACCESS_TOKEN,
            TWITTER_ACCESS_SECRET
        );
    }

    /**
     * Process all of the tweets, and do whatever is needed to win!
     *
     * @param array $tweets
     */
    public function process($tweets = array())
    {
        foreach ($tweets as $id => $tweet) {
            $text = $this->cleanText($tweet['status']);

            echo $this->requests;

            if ( ! $this->hasRequestsLeft()) {
                break;
            }

            if ($this->mustFavourite($text)) {
                $this->favorite($id);
            }

            if ($this->mustRetweet($text)) {
                $this->retweet($id);
            }

            if ($who = $this->mustFollow($text, $tweet['user'])) {
                $this->follow($who);
            }

        }
    }

    /**
     * Should the tweet be retweeted?
     *
     * @param $text
     * @return bool
     */
    protected function mustRetweet($text)
    {
        if (preg_match('/rt(.*)to win|retweet(.*)to win/i', $text)) {
            return true;
        }

        return false;
    }

    /**
     * Does the tweet need a favourite?
     *
     * @param $text
     * @return bool
     */
    protected function mustFavourite($text)
    {
        if (false !== mb_strpos($text, 'favorite') || false !== mb_strpos($text, 'favourite')) {
            return true;
        }

        return false;
    }

    /**
     * Do we need to follow the user?
     *
     * @param $text
     * @param $author
     * @return bool
     */
    protected function mustFollow($text, $author)
    {
        if (preg_match('/follow @+([a-z0-9_]{0,15})|follow/i', $text, $match)) {
            if (1 === count($match)) {
                return $author;
            } elseif (2 === count($match) && isset($match[1])) {
                return $match[1];
            }
        }

        return false;
    }

    /**
     * Retweet a status.
     *
     * @param $id
     * @return array|object
     */
    protected function retweet($id)
    {
        if ( ! $this->hasRequestsLeft()) {
            return false;
        }

        $this->requests++;

        return $this->oauth->post('statuses/retweet', [
            'id' => $id,
            'trim_user' => true
        ]);
    }

    /**
     * Favorite a status.
     *
     * @param $id
     * @return array|object
     */
    protected function favorite($id)
    {
        if ( ! $this->hasRequestsLeft()) {
            return false;
        }

        $this->requests++;

        return $this->oauth->post('favorites/create', [
            'id' => $id,
            'include_entities' => false
        ]);
    }

    /**
     * Follow user.
     *
     * @param $username
     * @return array|object
     */
    protected function follow($username)
    {
        if ( ! $this->hasRequestsLeft()) {
            return false;
        }

        $this->requests++;

        return $this->oauth->post('friendships/create', [
            'screen_name' => $username,
            'follow' => false
        ]);
    }

    // --

    /**
     * Simplify the text for a status.
     *
     * @param $tweet
     * @return string
     */
    protected function cleanText($tweet)
    {
        $text = str_replace('#', '', $tweet);
        $text = mb_strtolower($text);

        return trim($text);
    }

    /**
     * Do we have any requests left?
     *
     * @return bool
     */
    protected function hasRequestsLeft()
    {
        return $this->requests >= 14 ? false : true;
    }

} 